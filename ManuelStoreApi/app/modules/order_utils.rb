module OrderUtils
    
    #helpers methods
    def load_items_before(orderId)
        items = OrderItem.where(order_id: orderId)
        items_before = []
        items.each do |item|
            items_before << item.clone
        end
        items_before
    end

    # check methods
    def has_enough_stock_on_create(order_items, status)
        has_stock = true
        status_cancelado = 3

        if status != status_cancelado
            order_items.each do |item|
                product = Product.find(item['product_id'])
                if product.stock < item['quantity']
                    has_stock = false
                end
            end
        end
        has_stock
    end

    #Check if there is enough stock so the order can be updated
    def has_enough_stock_on_update(order_items, status, status_before, orderId)
        has_stock = true
        status_cancelado = 3

        if !status.present?
            status = status_before
        end

        if status != status_cancelado && !order_items.blank?
            order_items.each do |item|
                product = Product.find(item['product_id'])
                quantity = 0
                if !item['id'].blank? && status_before != status_cancelado
                    quantity = OrderItem.find(item['id']).quantity
                end

                if product.stock + quantity < item['quantity'] && !item['_destroy']
                    has_stock = false
                end
            end
        end

        #checking the db items, if they were cancel then they can make difference in stock right now, otherwise they were already computed
        itemsDb = OrderItem.where(order_id: orderId)
        if !itemsDb.blank? && status_before == status_cancelado && status != status_cancelado
            itemsDb.each do |item|
                product = Product.find(item.product_id)
                check = true
                if !order_items.blank? 
                    order_items.each do |it|
                        if it['product_id'] == item.product_id
                            #already checked with the include items
                            check = false
                        end
                    end
                end

                if check 
                    if product.stock < item.quantity
                        has_stock = false
                    end
                end
            end
        end

        has_stock
    end

    #update the stock after the order
    def update_stock_on_create(order_items, status)
        status_cancelado = 3
        if status != status_cancelado
            order_items.each do |item|
                product = Product.find(item['product_id'])
                stock = product.stock - item['quantity']
                product.update_attribute(:stock, stock)
            end
        end
    end

    #update the stock after the update of the order
    def update_stock_on_update(order_items, items_before, status, status_before, orderId)
        status_cancelado = 3

        if !status.present?
            status = status_before
        end

        if !order_items.blank?
            order_items.each do |item|
                quantity_before = 0
                if !item['id'].blank?
                    if status_before != status_cancelado
                        items_before.each do |item_before|
                            if item_before.id == item['id']
                                quantity_before = item_before.quantity
                            end
                        end
                    end
                end
                quantity_after = 0
                if ((status != status_cancelado) && !item['_destroy'])
                    quantity_after = item['quantity']
                end

                #when deleting the item['quantity'] must be zero
                product = Product.find(item['product_id'])
                stock = product.stock - quantity_after + quantity_before
                product.update_attribute(:stock, stock)
            end
        end

        #checking the db items, if they were cancel then they can make difference in stock right now, otherwise they were already computed
        itemsDb = OrderItem.where(order_id: orderId)
        if !itemsDb.blank? 
            itemsDb.each do |item|
                product = Product.find(item.product_id)
                check = true
                if !order_items.blank? 
                    order_items.each do |it|
                        if it['product_id'] == item.product_id
                            #already checked with the include items
                            check = false
                        end
                    end
                end

                if check 
                    stock = product.stock
                    if status_before == status_cancelado && status != status_cancelado
                        stock = stock - item.quantity
                    end
                    if status_before != status_cancelado && status == status_cancelado
                        stock = stock + item.quantity
                    end
                    product.update_attribute(:stock, stock)
                end
            end
        end
    end

    def has_items(order_items)
        has = true
        if order_items.blank?
            has = false
        else
            order_items.each do |item|
                if !Product.exists?(item['product_id'])
                    has = false
                end
            end
        end
        has
    end

    def products_without_stock_on_create(order_items)
        items_list = []
        order_items.each do |item|
            product = Product.find(item['product_id'])
            if product.stock < item['quantity']
                items_list << (product.name + ' stock: ' + product.stock.to_s)
            end
        end

        items_list
    end

    def products_without_stock_on_update(order_items, items_before, status, status_before, orderId)
        status_cancelado = 3
        items_list = []
        if !status.present?
            status = status_before
        end
        #Checking the insert items
        if !order_items.blank? && status != status_cancelado
            order_items.each do |item|
                product = Product.find(item['product_id'])
                quantity_before = 0
                if !item['id'].blank?
                    if status_before != status_cancelado
                        quantity_before = OrderItem.find(item['id']).quantity
                    end
                end
                if product.stock + quantity_before < item['quantity'] && !item['_destroy']
                    items_list << (product.name + ' stock: ' + (product.stock + quantity_before).to_s)
                end
            end
        end

        #checking the db items, if they were cancel then they can make difference in stock right now, otherwise they were already computed
        itemsDb = OrderItem.where(order_id: orderId)
        if !itemsDb.blank? && status_before == status_cancelado && status != status_cancelado
            itemsDb.each do |item|
                product = Product.find(item.product_id)
                check = true
                if !order_items.blank? 
                    order_items.each do |it|
                        if it['product_id'] == item.product_id
                            #already checked with the include items
                            check = false
                        end
                    end
                end

                if check 
                    if product.stock < item.quantity
                        items_list << (product.name + ' stock: ' + (product.stock).to_s)
                    end
                end
            end
        end

        items_list
    end
end