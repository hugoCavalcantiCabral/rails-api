module Api
	module V1
		class ProductsController < ApplicationController
			rescue_from ActiveRecord::RecordNotFound, with: :record_not_found_rescue

			# dealing with RecordNotFound exception
			def record_not_found_rescue(exception)
				logger.info("#{exception.class}: " + exception.message)
				if Rails.env.production?
				  render json: {}, status: :not_found
				else
				  render json: { message: exception }, status: :not_found
				end
			end

			# GET /api/v1/products
			def index
				products = Product.includes(:product_attributes).order('created_at DESC');
				render json: {status: 'SUCCESS', message:'Loaded products', data:products }, include: :product_attributes,status: :ok
			end
			# GET /api/v1/products/:id
			def show
				product = Product.includes(:product_attributes).find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded product', data:product}, include: :product_attributes, status: :ok
			end
			# POST /api/v1/products
			def create
				product = Product.new(product_params)
				if product.save
					render json: {status: 'SUCCESS', message:'Saved product', data:product}, include: :product_attributes, status: :ok
				else
					render json: {status: 'ERROR', message:'Product not saved', data:product.errors},status: :unprocessable_entity
				end
			end
			# DELETE /api/v1/products/:id
			def destroy
				product = Product.includes(:product_attributes).find(params[:id])
				if product.destroy
					render json: {status: 'SUCCESS', message:'Deleted product', data:product}, include: :product_attributes, status: :ok
				else
					render json: {status: 'ERROR', message:'Product not deleted', data:product.errors},status: :unprocessable_entity
				end
			end
			# PUT /api/v1/products/:id
			def update
				product = Product.includes(:product_attributes).find(params[:id])
				if product.update(product_params)
					render json: {status: 'SUCCESS', message:'Updated product', data:product}, include: :product_attributes, status: :ok
				else
					render json: {status: 'ERROR', message:'Product not update', data:product.errors},status: :unprocessable_entity
				end
			end
			# accepted params
			private
			def product_params
				params.require(:product).permit(:code, :name, :description, :stock, :price, 
					product_attributes_attributes: [:id, :description, :value, :_destroy])
			end
		end
	end
end