module Api
	module V1
        class OrdersController < ApplicationController
			rescue_from ActiveRecord::RecordNotFound, with: :record_not_found_rescue

			include OrderUtils

			# dealing with RecordNotFound exception
			def record_not_found_rescue(exception)
				logger.info("#{exception.class}: " + exception.message)
				if Rails.env.production?
				  render json: {}, status: :not_found
				else
				  render json: { message: exception }, status: :not_found
				end
			end

      		# GET /api/v1/orders
			def index
				orders = Order.includes(:order_items).order('created_at DESC');
				render json: {status: 'SUCCESS', message:'Loaded orders', data:orders }, include: :order_items,status: :ok
			end
			# GET /api/v1/orders/:id
			def show
				order = Order.includes(:order_items).find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded order', data:order}, include: :order_items, status: :ok
			end
			# POST /api/v1/orders
			def create
				order = Order.new(order_attributes)

				if has_items(order_attributes[:order_items_attributes])
					if has_enough_stock_on_create(order_attributes[:order_items_attributes], order_attributes[:status])
						if order.save
							update_stock_on_create(order_attributes[:order_items_attributes], order_attributes[:status])
							render json: {status: 'SUCCESS', message:'Saved order', data:order}, include: :order_items, status: :ok
						else
							render json: {status: 'ERROR', message:'Order not saved', data:order.errors},status: :unprocessable_entity
						end
					else
						render json: {status: 'ERROR', message:('Order not saved, products without enough stock: ' + products_without_stock_on_create(order_attributes[:order_items_attributes]).join(', '))},status: :unprocessable_entity
					end
				else
					render json: {status: 'ERROR', message:'Empty order or invalid items'},status: :unprocessable_entity
				end
			end
			# When updating does not check if there is items, because can be only a update of status, or whatever excluding items
			# PUT /api/v1/orders/:id
			def update
				order = Order.includes(:order_items).find(params[:id])
				items_before = load_items_before(order.id)
				status_before = Order.statuses[order.status]
				if has_enough_stock_on_update(order_attributes[:order_items_attributes], order_attributes[:status], status_before, order.id)
					if order.update(order_attributes)
						update_stock_on_update(order_attributes[:order_items_attributes], items_before, order_attributes[:status], status_before, order.id)
						render json: {status: 'SUCCESS', message:'Updated order', data:order}, include: :order_items, status: :ok
					else
						render json: {status: 'ERROR', message:'Order not update', data:order.errors},status: :unprocessable_entity
					end
				else
					render json: {status: 'ERROR', message:('Order not saved, products without enough stock: ' + products_without_stock_on_update(order_attributes[:order_items_attributes], items_before, order_attributes[:status], status_before, order.id).join(', '))},status: :unprocessable_entity
				end
			end

			# accepted params
			private
			def order_attributes
				params.require(:order).permit(:code, :buy_date, :buyer_name, :status, :delivery_price,
					order_items_attributes: [:id, :product_id, :quantity, :sale_price, :_destroy])
			end
		end
	end
end