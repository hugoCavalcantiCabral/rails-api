module Api
	module V1
        class AverageTicketsController < ApplicationController
            rescue_from ActionController::ParameterMissing, with: :parameter_not_found_rescue

            # dealing with ParameterMissing exception
			def parameter_not_found_rescue(exception)
				logger.info("#{exception.class}: " + exception.message)
				if Rails.env.production?
				  render json: {}, status: :not_found
				else
				  render json: { message: exception }, status: :not_found
				end
			end

            #POST /api/v1/reportAverageTickets
            def report
                orders = Order.where(buy_date: DateTime.parse(report_params[:start]).beginning_of_day..DateTime.parse(report_params[:end]).end_of_day)
                count = orders.count
                total_ordered = calc_total_ordered(orders)

                response = { start: report_params[:start], end: report_params[:end], orders_count: count, total_ordered: total_ordered, average_ticket: (if count != 0 then total_ordered/count else 0 end) }

                render json: {status: 'SUCCESS', message:'Report Generated', data: response },status: :ok
            end

            def calc_total_ordered(orders)
                total = 0.0

                if !orders.blank?
                    orders.each do |order|
                        order.order_items.each do |item|
                            total += (item.sale_price * item.quantity)
                        end
                    end
                end

                total
            end

            # accepted params
			private
            def report_params
                params.require(:reportAverageTicket).permit(:start, :end).tap do |parameter|
                    parameter.require(:start)
                    parameter.require(:end)
                end
			end
        end
    end
end