# This class will carry any kind of attribute of the products, we only need to inform the type(size, shape, weight...)
# and the value of the attribute, the "value" column its a string type so it could carry any kind of value, then the
# validation of the data should be implement before insert

class ProductAttribute < ApplicationRecord
  belongs_to :product

  validates :description, :value, presence: true
end
