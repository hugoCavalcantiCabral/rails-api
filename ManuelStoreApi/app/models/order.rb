class Order < ApplicationRecord
    # List with all possible status, then the column value is in the range [0, 3]
    enum status: [ :novo, :aprovado, :entregue, :cancelado ]

    has_many :order_items
    has_many :products, through: :order_items
    accepts_nested_attributes_for :order_items, allow_destroy: true

    validates :code, :buy_date, :buyer_name, :status, :delivery_price, presence: true
    validates :code, uniqueness: true
    validates :status, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 3 }
    validates :delivery_price, numericality: { greater_than_or_equal_to: 0.0 }
end
