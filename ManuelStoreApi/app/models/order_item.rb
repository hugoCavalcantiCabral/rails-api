class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  validates :product_id, :quantity, :sale_price, presence: true
  validates :quantity, numericality: { greater_than: 0 }
  validates :sale_price, numericality: { greater_than: 0.0 }
end
