class Product < ApplicationRecord

    # Can't delete product if there is any associated order  
    has_many :order_items, dependent: :restrict_with_error
    has_many :orders, through: :order_items
    has_many :product_attributes, dependent: :destroy
    accepts_nested_attributes_for :product_attributes, allow_destroy: true
    
    # Price wont be validate because we may only want to set the real value (column sale_price in order_item)
    validates :code, :name, :description, :stock, presence: true
    validates :code, uniqueness: true
    validates :stock, numericality: { greater_than_or_equal_to: 0 }
end
