# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts 'Initializing products without attributes'
30.times  do
	Product.create!({
		code: Faker::Number.unique.decimal_part(10),
        name: Faker::Food.dish,
        description: Faker::Food.description,
        stock: Faker::Number.between(0, 20),
        price: Faker::Number.decimal(2, 2)
	})
end
puts 'End'

puts 'Initializing products with attributes'
20.times  do
	Product.create!({
		code: Faker::Number.unique.decimal_part(10),
        name: Faker::Food.dish,
        description: Faker::Food.description,
        stock: Faker::Number.between(0, 20),
        price: Faker::Number.decimal(2, 2),
        product_attributes: [
            ProductAttribute.new(description: "Ingredient", value: Faker::Food.ingredient),
            ProductAttribute.new(description: "Measure", value: Faker::Food.measurement)
        ]
    })
end
puts 'End'

puts 'Initializing orders'
30.times  do
	Order.create!({ 
        code: Faker::Number.unique.decimal_part(10),
        buy_date: Faker::Date.between(2.year.ago, Date.today),
        buyer_name: Faker::Name.name,
        status: Faker::Number.between(0, 3),
        delivery_price: Faker::Number.decimal(3, 2),
        order_items: [
            OrderItem.new(product_id: Product.order("RANDOM()").limit(1).first.id, 
                quantity: Faker::Number.between(1, 20), sale_price: Faker::Number.decimal(2, 2)),
            OrderItem.new(product_id: Product.order("RANDOM()").limit(1).first.id,
                quantity: Faker::Number.between(1, 20), sale_price: Faker::Number.decimal(2, 2)),
            OrderItem.new(product_id: Product.order("RANDOM()").limit(1).first.id, 
                quantity: Faker::Number.between(1, 20), sale_price: Faker::Number.decimal(2, 2)),
            OrderItem.new(product_id: Product.order("RANDOM()").limit(1).first.id, 
                quantity: Faker::Number.between(1, 20), sale_price: Faker::Number.decimal(2, 2)),
            OrderItem.new(product_id: Product.order("RANDOM()").limit(1).first.id, 
                quantity: Faker::Number.between(1, 20), sale_price: Faker::Number.decimal(2, 2))
        ]
	})
end
puts 'End'