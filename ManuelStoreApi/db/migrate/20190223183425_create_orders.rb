class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :code
      t.datetime :buy_date
      t.string :buyer_name
      t.integer :status
      t.float :delivery_price

      t.timestamps
    end
  end
end
