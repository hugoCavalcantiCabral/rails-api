class CreateProductAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :product_attributes do |t|
      t.belongs_to :product, foreign_key: true
      t.string :description
      t.string :value

      t.timestamps
    end
  end
end
