require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  test "invalid order by code" do
    order = Order.new

    #order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by empty code" do
    order = Order.new

    order.code= ""
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by repeated code" do
    order = Order.new

    order.code= orders(:one).code
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by buy_date" do
    order = Order.new

    order.code= "12345"
    #order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by empty buy_date" do
    order = Order.new

    order.code= "12345"
    order.buy_date = ""
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by invalid buy_date" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "teste de data invalida"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by buyer_name" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    #order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by empty buyer_name" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = ""
    order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by status" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    #order.status = 2
    order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end

  test "invalid order by max status" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.delivery_price = 10
    
    assert_raises (ArgumentError) {order.status = 4}

    assert_not order.save

    order.destroy!
  end

  test "invalid order by min status" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.delivery_price = 10

    assert_raises (ArgumentError) {order.status = -1}

    assert_not order.save

    order.destroy!
  end

  test "invalid order by delivery_price" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    #order.delivery_price = 10

    assert_not order.save

    order.destroy!
  end
  
  test "invalid order by negative delivery_price" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = -3

    assert_not order.save

    order.destroy!
  end

  test "valid order by delivery_price zero" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 0.000

    assert order.save

    order.destroy!
  end

  test "valid fields after create" do
    order = Order.new

    order.code= "12345"
    order.buy_date = "2019-02-23 15:34:25"
    order.buyer_name = "MyString"
    order.status = 2
    order.delivery_price = 20.0200
    
    assert order.save

    assert_not_nil order.id
    assert_not_nil order.code
    assert_not_nil order.buy_date
    assert_not_nil order.buyer_name
    assert_not_nil order.status
    assert_not_nil order.delivery_price
    assert_not_nil order.created_at
    assert_not_nil order.updated_at
  end
end
