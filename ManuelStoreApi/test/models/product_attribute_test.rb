require 'test_helper'

class ProductAttributeTest < ActiveSupport::TestCase
  test "invalid product attribute by product" do
    attribute = ProductAttribute.new

    #attribute.product_id = 
    attribute.description = "DESCRIÇÃO TESTE"
    attribute.value = "VALOR TESTE"

    assert_not attribute.save

    attribute.destroy!
  end

  test "invalid product attribute by inexistent product" do
    attribute = ProductAttribute.new

    attribute.product_id = 11111111111111
    attribute.description = "DESCRIÇÃO TESTE"
    attribute.value = "VALOR TESTE"

    assert_not attribute.save

    attribute.destroy!
  end

  test "invalid product attribute by description" do
    attribute = ProductAttribute.new

    attribute.product_id = products(:one).id
    #attribute.description = "DESCRIÇÃO TESTE"
    attribute.value = "VALOR TESTE"

    assert_not attribute.save

    attribute.destroy!
  end

  test "invalid product attribute by empty description" do
    attribute = ProductAttribute.new

    attribute.product_id = products(:one).id
    attribute.description = ""
    attribute.value = "VALOR TESTE"

    assert_not attribute.save

    attribute.destroy!
  end

  test "invalid product attribute by value" do
    attribute = ProductAttribute.new

    attribute.product_id = products(:one).id
    attribute.description = "DESCRIÇÃO TESTE"
    #attribute.value = ""

    assert_not attribute.save

    attribute.destroy!
  end

  test "invalid product attribute by empty value" do
    attribute = ProductAttribute.new

    attribute.product_id = products(:one).id
    attribute.description = "DESCRIÇÃO TESTE"
    attribute.value = ""

    assert_not attribute.save

    attribute.destroy!
  end

  test "valid fields after create" do
    attribute = ProductAttribute.new

    attribute.product_id = products(:one).id
    attribute.description = "DESCRIÇÃO TESTE"
    attribute.value = "VALOR TESTE"
    
    assert attribute.save

    assert_not_nil attribute.id
    assert_not_nil attribute.product_id
    assert_not_nil attribute.description
    assert_not_nil attribute.value
    assert_not_nil attribute.created_at
    assert_not_nil attribute.updated_at

    product = Product.find(attribute.product_id)

    assert product
  end
end
