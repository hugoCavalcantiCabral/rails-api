require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test "invalid product by code" do
    product = Product.new

    #product.code= "12345"
    product.name = "MyString"
    product.description = "MyString"
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by empty code" do
    product = Product.new

    product.code = ""
    product.name = "MyString"
    product.description = "MyString"
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by repeated code" do
    product = Product.new

    product.code = products(:one).code
    product.name= "MyString"
    product.description= "MyString"
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by name" do
    product = Product.new

    product.code = "12345"
    #product.name = "MyString"
    product.description = "MyString"
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by empty name" do
    product = Product.new

    product.code = "12345"
    product.name = ""
    product.description = "MyString"
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by description" do
    product = Product.new

    product.code = "12345"
    product.name = "MyString"
    #product.description = "MyString"
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by empty description" do
    product = Product.new

    product.code = "12345"
    product.name = "MyString"
    product.description = ""
    product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by stock" do
    product = Product.new

    product.code = "12345"
    product.name = "MyString"
    product.description = "MyString"
    #product.stock = 1.5
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "invalid product by negative stock" do
    product = Product.new

    product.code = "12345"
    product.name = "MyString"
    product.description = "MyString"
    product.stock = -2
    product.price = 1.5

    assert_not product.save

    product.destroy!
  end

  test "valid product without price" do
    product = Product.new

    product.code = "12345"
    product.name = "MyString"
    product.description = "MyString"
    product.stock = 1.5
    #product.price = 1.5

    assert product.save

    product.destroy!
  end

  test "valid fields after create" do
    product = Product.new

    product.code = "12345"
    product.name = "MyString"
    product.description = "MyString"
    product.stock = 1.5
    product.price = 1.5
    
    assert product.save

    assert_not_nil product.id
    assert_not_nil product.code
    assert_not_nil product.name
    assert_not_nil product.description
    assert_not_nil product.stock
    assert_not_nil product.price
    assert_not_nil product.created_at
    assert_not_nil product.updated_at
  end
end
