require 'test_helper'

class OrderItemTest < ActiveSupport::TestCase
   test "invalid item by sale price" do
     item = OrderItem.new

     item.product_id = products(:two).id
     item.quantity = 2
     #item.sale_price = 2
     item.order_id = orders(:one).id

     assert_not item.save

     item.destroy!
   end

   test "invalid item by sale price zero" do
    item = OrderItem.new

    item.product_id = products(:two).id
    item.quantity = 2
    item.sale_price = 0.0
    item.order_id = orders(:one).id

    assert_not item.save

    item.destroy!
  end

   test "invalid item by quantity" do
    item = OrderItem.new

    item.product_id = products(:two).id
    #item.quantity = 2
    item.sale_price = 2
    item.order_id = orders(:one).id

    assert_not item.save

    item.destroy!
  end

  test "invalid item by quantity zero" do
    item = OrderItem.new

    item.product_id = products(:two).id
    item.quantity = 0
    item.sale_price = 2
    item.order_id = orders(:one).id

    assert_not item.save

    item.destroy!
  end

  test "invalid item by product" do
    item = OrderItem.new

    #item.product_id = products(:two).id
    item.quantity = 2
    item.sale_price = 2
    item.order_id = orders(:one).id

    assert_not item.save

    item.destroy!
  end

  test "invalid item by inexistent product" do
    item = OrderItem.new

    item.product_id = 11111111111
    item.quantity = 2
    item.sale_price = 2
    item.order_id = orders(:one).id

    assert_not item.save

    item.destroy!
  end

  test "invalid item by order" do
    item = OrderItem.new

    item.product_id = products(:two).id
    item.quantity = 2
    item.sale_price = 2
    #item.order_id = orders(:one).id

    assert_not item.save

    item.destroy!
  end

  test "invalid item by inexistent order" do
    item = OrderItem.new

    item.product_id = products(:two).id
    item.quantity = 2
    item.sale_price = 2
    item.order_id = 111111111111

    assert_not item.save

    item.destroy!
  end

  test "valid fields after create" do
    item = OrderItem.new

    item.product_id = products(:two).id
    item.quantity = 2
    item.sale_price = 2
    item.order_id = orders(:one).id
    
    assert item.save

    assert_not_nil item.id
    assert_not_nil item.product_id
    assert_not_nil item.quantity
    assert_not_nil item.sale_price
    assert_not_nil item.order_id
    assert_not_nil item.created_at
    assert_not_nil item.updated_at

    product = Product.find(item.product_id)
    order = Order.find(item.order_id)

    assert product
    assert order
  end
end
