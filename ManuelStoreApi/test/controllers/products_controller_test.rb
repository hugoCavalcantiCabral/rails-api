require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
    @attribute1 = product_attributes(:one)
    @attribute2 = product_attributes(:anotherOne)
    @product2 = products(:stock5)
  end

  test "should get index" do
    get api_v1_products_url, as: :json
    data = JSON.parse(@response.body)
    
    assert_response :success
    assert_same(data["data"].size, 6)
  end

  test "should create product" do
    assert_difference('Product.count') do
      post api_v1_products_url, params: { "product":{
          "code": "0001122222332",
          "name": "TESTE DE PRODUTO NOVO",
          "description": "ARROZ COM QUEIJO",
          "stock": 432,
          "price": 21,
          "product_attributes_attributes": [
              {
                  "description": "PESO DO PRODUTO",
                  "value": "12KG"
              },
              {
                  "description": "SABOR DO PRODUTO",
                  "value": "DELICIOSO"
              }
          ]
      } }, as: :json
    end
    data = JSON.parse(@response.body)

    assert_response :success
    assert_same(data["data"]["product_attributes"].size, 2)
  end

  test "should show product" do
    get api_v1_product_url(@product), as: :json
    data = JSON.parse(@response.body)

    assert_response :success
    assert_same(data["data"]["product_attributes"].size, 2)
  end

  test "should update product" do
    patch api_v1_product_url(@product), params: { "product":{
        "code": "123412",
        "name": "NOVO NOME",
        "description": "ARROZ COM QUEIJO",
        "stock": 100,
        "price": 21,
        "product_attributes_attributes": [
            {
                "description": "NOVO SABOR DO PRODUTO!",
                "value": "MAIS DELICIOSO"
            },
            {
                "id": @attribute1.id,
                "description": "PESO DO PRODUTO",
                "value": "122KG"
            },
            {
              "id": @attribute2.id,
              "_destroy": true
            }
        ]
    } }, as: :json
    data = JSON.parse(@response.body)

    assert_response 200
    assert_same(data["data"]["product_attributes"].size, 2)
    assert data["data"]["name"] == "NOVO NOME"
  end


  test "should not destroy product because exists order" do
    assert_no_difference('Product.count') do
      delete api_v1_product_url(@product), as: :json
    end

    assert_response 422
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete api_v1_product_url(@product2), as: :json
    end

    assert_response 200
  end
end
