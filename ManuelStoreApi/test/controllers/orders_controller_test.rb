require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
    @orderCancel = orders(:cancel)
    @product1 = products(:one)
    @product2 = products(:two)
    @productStock50 = products(:stock50)
    @productStock110 = products(:stock110)
    @item1 = order_items(:one)
    @item2 = order_items(:quantity100)
    @item3 = order_items(:quantity10)
    @itemCancel = order_items(:itemOfCancelOrder)
  end

  test "should get index" do
    get api_v1_orders_url, as: :json
    data = JSON.parse(@response.body)
    
    assert_response :success
    assert_same(data["data"].size, 3)
  end

  test "should create order" do
    assert_difference('Order.count') do
      post api_v1_orders_url, params: { "order": { 
        "code": "112358",
        "buy_date": "2019-02-24 10:31:03",
        "buyer_name": "Pedido de exemplo de entrada",
        "status": 1,
        "delivery_price": 12.32,
        "order_items_attributes": [
          {
            "product_id": @product1.id, 
            "quantity": 1,
            "sale_price": 10.00
          },
          {
            "product_id": @product2.id, 
            "quantity": 0.7,
            "sale_price": 10.00
          }
        ]
      } }, as: :json
    end
    data = JSON.parse(@response.body)

    product1New = Product.find(@product1.id)
    product2New = Product.find(@product2.id)

    assert_response 200
    assert_same(data["data"]["order_items"].size, 2)
    assert_same(product1New.stock, 0.5)
    assert_same(product2New.stock, 0.8)
  end

  test "should create cancelled order" do
    assert_difference('Order.count') do
      post api_v1_orders_url, params: { "order": { 
        "code": "112358",
        "buy_date": "2019-02-24 10:31:03",
        "buyer_name": "Pedido de exemplo de entrada",
        "status": 3,
        "delivery_price": 12.32,
        "order_items_attributes": [
          {
            "product_id": @product1.id, 
            "quantity": 122,
            "sale_price": 10.00
          },
          {
            "product_id": @product2.id, 
            "quantity": 1500,
            "sale_price": 10.00
          }
        ]
      } }, as: :json
    end
    data = JSON.parse(@response.body)

    assert_response 200
    assert_same(data["data"]["order_items"].size, 2)
  end

  test "should show order" do
    get api_v1_order_url(@order), as: :json
    data = JSON.parse(@response.body)

    assert_response :success
    assert_same(data["data"]["order_items"].size, 3)
  end

  test "should update order" do
    patch api_v1_order_url(@order), params: { order: { "code": "11111",
      "buy_date": "2019-03-03T10:31:03.000Z",
      "buyer_name": "NOVO COMPRADOR",
      "status": 3,
      "delivery_price": 12.32,
      "order_items_attributes": [
          {
              "id": @item1.id,
              "product_id": @product1.id,
              "quantity": 400,
              "sale_price": 5
          },
          {
              "id": @item2.id,
              "product_id": @product2.id,
              "quantity": 500,
              "sale_price": 0.1
          }
      ] } }, as: :json

    data = JSON.parse(@response.body)

    assert_response 200
    assert_same(data["data"]["order_items"].size, 3)
    assert data["data"]["buyer_name"] == "NOVO COMPRADOR"
  end

  test "should not update order by stock" do
    patch api_v1_order_url(@orderCancel), params: { order: { "code": "11111",
      "buyer_name": "NOVO COMPRADOR",
      "status": 2
    } }, as: :json

    assert_response 422
  end

  test "should update order and increase stock" do
    patch api_v1_order_url(@order), params: { order: {
      "buyer_name": "NOVO COMPRADOR",
      "status": 3
    } }, as: :json

    product1New = Product.find(@product1.id)
    productStock50New = Product.find(@productStock50.id)
    productStock110New = Product.find(@productStock110.id)

    assert_response 200
    assert_same(product1New.stock, @product1.stock + @item1.quantity)
    assert_same(productStock110New.stock, @productStock110.stock + @item2.quantity)
    assert_same(productStock50New.stock, @productStock50.stock + @item3.quantity)
  end

end
