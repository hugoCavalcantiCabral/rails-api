require 'test_helper'

class AverageTicketsControllerTest < ActionDispatch::IntegrationTest

  test "should generate report average_ticket" do
    
    post api_v1_reportAverageTickets_url, params: { reportAverageTicket: { start: '2019-02-20', end: '2019-02-22' } }, as: :json
    data = JSON.parse(@response.body)

    assert_response 200
    assert_same(data['data']['orders_count'], 1)
    assert_same(data['data']['total_ordered'], 1157.25)
  end

  test "should generate report average_ticket 2" do
    
    post api_v1_reportAverageTickets_url, params: { reportAverageTicket: { start: '2019-02-19', end: '2019-02-23' } }, as: :json
    data = JSON.parse(@response.body)

    assert_response 200
    assert_same(data['data']['orders_count'], 2)
    assert_same(data['data']['total_ordered'], 1159.5)
  end
end
