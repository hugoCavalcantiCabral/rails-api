# Desafio - SkyHub

# Configurações do Ambiente
	ruby 2.5.1
	Rails 5.2.2
	PostgreSQL 10.6
	Ubuntu 18.04
	
# Informações Adicionais
Para escolha das ferramentas de desenvolvimento levei em consideração o que a recrutadora me informou, que a tecnologia usada para a posição era Ruby, como durante nossa conversa não foi informado nada sobre o BD então optei pelo PostgresSQL, apenas por maior familiaridade com a ferramenta.

Também escolhi a linguagem de programação por um segundo motivo, eu não a conhecia, sendo assim ao desenvolver em Ruby também quis mostrar a empresa que tenho capacidade e desejo de aprender novas tecnologias. (Por sinal foi uma experiência muito boa :) )

# Regras de negócio adicionais
Para dar maior consistência a aplicação foram adicionadas algumas regras de negócio:

* Não é possível deletar um pedido (pois não constava como uma das ações nos requisitos do desafio), mas é possível "inativar" mudando o status do pedido para 3(cancelado), esse status não influência em estoque

* Não é permitido deletar um produto se houver um item de algum pedido em que ele conste, para efeitos de histórico e auditoria

* os Status possiveis para um pedido são: 0 (novo), 1 (aprovado), 2(entregue), 3(cancelado)

* Não é permitido valores negativos para os campos: quantidade, preço de venda (nos itens do pedido), frete (no pedido), estoque (no produto)

* Não haverá validação de estoque quando os pedidos forem criados ou alterados para o status 3 (cancelado)

* O único campo não obrigatório é o preço nos produtos (pois foi assumido que seria apenas um preço base, o preço real seria definido na venda, e ficaria salvo nos itens do pedido)

* Para os atributos variáveis do produto foi criada uma nova tabela "product_attributes", que possue a relação de many-to-one com o produto, sendo assim possível cadastrar vários atributos com descrição e valor para os produtos

* A estrutura dos "product_attributes" é a seguinte: product_id (identificação do produto), description (descrição do atributo), value (valor do atributo), o campo "value" é uma string, para ser permitido a inserção de qualquer tipo de dado, sendo assim, o tratamento sobre o dado seria feito nos devidos casos de uso

# Instalação
- Clonar o repositório, e:

	cd code-challenge/ManuelStoreApi/
	
	bundle install
	
	rails db:create
	
	rails db:migrate
	
OBS: será necessário modificar o arquivo database.yml para a aplicação ter acesso ao banco. Modificar o "username", "password" e "host"
	
# Rodar a aplicação e testes
- Para rodar o gerador de massa inicial, executar:

	rails db:seed

- Para rodar a aplicação, executar:

	rails s

- Para rodar os testes, executar:

	rails test

#Exemplos de entrada e saída
- Os exemplos de entrada e saída foram salvos nos arquivos

	Exemplo Relatório Ticket.txt
	
	Exemplos Pedidos.txt
	
	Exemplos Produtos.txt
	
- Dentro dos arquivos, quando necessário, existem possíveis cenários de testes, usados durante o desenvolvimento e testes da aplicação
