Exemplos de cadastrar/alterar/recuperar/excluir produtos

Listar todos os produtos já criados
entrada:
GET /api/v1/products

saída:
{
    "status": "SUCCESS",
    "message": "Loaded products",
    "data": [
        {
            "id": 110,
            "code": "0444",
            "name": "produto teste",
            "description": "teste",
            "stock": 21,
            "price": 900302.08,
            "created_at": "2019-02-25T08:29:12.923Z",
            "updated_at": "2019-02-25T08:29:12.923Z",
            "product_attributes": [
                {
                    "id": 96,
                    "product_id": 110,
                    "description": "altura",
                    "value": "12",
                    "created_at": "2019-02-25T08:29:12.925Z",
                    "updated_at": "2019-02-25T08:29:12.925Z"
                }
            ]
        }
}

Listar produto baseado no id
Cenário de teste:
- Buscar por produto com id inexistente

entrada:
GET /api/v1/products/:id

saída:
{
    "status": "SUCCESS",
    "message": "Loaded product",
    "data": {
        "id": 105,
        "code": "123223",
        "name": "TESTE DE HUGO 2",
        "description": "TESTE TESTE STE STESFDSJFKLSD",
        "stock": 321,
        "price": null,
        "created_at": "2019-02-24T20:43:04.768Z",
        "updated_at": "2019-02-24T20:43:04.768Z",
        "product_attributes": [
            {
                "id": 95,
                "product_id": 105,
                "description": "TESTE DE DESCRIÇÃO DO 11112",
                "value": "TESTE DO VALOR DO 12",
                "created_at": "2019-02-24T20:43:04.773Z",
                "updated_at": "2019-02-24T20:43:04.773Z"
            },
            {
                "id": 94,
                "product_id": 105,
                "description": "TESTE DE DESCRIÇÃO DO 112",
                "value": "TESTE DO VALOR DO 12",
                "created_at": "2019-02-24T20:43:04.771Z",
                "updated_at": "2019-02-24T20:43:04.771Z"
            },
            {
                "id": 93,
                "product_id": 105,
                "description": "TESTE DE DESCRIÇÃO DO 12",
                "value": "TESTE DO VALOR DO 12",
                "created_at": "2019-02-24T20:43:04.770Z",
                "updated_at": "2019-02-24T20:43:04.770Z"
            }
        ]
    }
}

Inserir produto
  
Cenários de teste:
- Inserir produto faltando: code, name, description, stock
- Inserir produto com "code" repetido
- Inserir produto com "stock" menor que zero
- Inserir atributo do produto faltando: description, value

Entrada:
POST /api/v1/products
{
	"product":{
        "code": "0001122222332",
        "name": "TESTE DE PRODUTO NOVO",
        "description": "ARROZ COM QUEIJO",
        "stock": 432,
        "price": 21,
        "product_attributes_attributes": [
            {
                "description": "PESO DO PRODUTO",
                "value": "12KG"
            },
            {
                "description": "SABOR DO PRODUTO",
                "value": "DELICIOSO"
            }
        ]
    }
}

Saída:
{
    "status": "SUCCESS",
    "message": "Saved product",
    "data": {
        "id": 115,
        "code": "0001122222332",
        "name": "TESTE DE PRODUTO NOVO",
        "description": "ARROZ COM QUEIJO",
        "stock": 432,
        "price": 21,
        "created_at": "2019-02-27T08:41:23.665Z",
        "updated_at": "2019-02-27T08:41:23.665Z",
        "product_attributes": [
            {
                "id": 106,
                "product_id": 115,
                "description": "SABOR DO PRODUTO",
                "value": "DELICIOSO",
                "created_at": "2019-02-27T08:41:23.670Z",
                "updated_at": "2019-02-27T08:41:23.670Z"
            },
            {
                "id": 105,
                "product_id": 115,
                "description": "PESO DO PRODUTO",
                "value": "12KG",
                "created_at": "2019-02-27T08:41:23.667Z",
                "updated_at": "2019-02-27T08:41:23.667Z"
            }
        ]
    }
}

Atualizar pedido
Cenários de teste:
- Atualizar o "code" para um outro já existente em outro pedido
- Atualizar o "stock" para valor menor que zero
OBS: Para atualizar basta a modificação de um atributo, não será necessário preencher todos os campos novamente
OBS: É possível remover um atributo dos produtos, adicionando o campo "_destroy": true

Entrada:
PUT /api/v1/products/:id
{
	"product":{
        "code": "123412",
        "name": "TESTE DE PRODUTO NOVO",
        "description": "ARROZ COM QUEIJO",
        "stock": 42,
        "price": 21,
        "product_attributes_attributes": [
            {
                "id": 106,
                "description": "SABOR DO PRODUTO!",
                "value": "DELICIOSO"
            },
            {
                "id": 105,
                "description": "PESO DO PRODUTO",
                "value": "12KG"
            }
        ]
    }
}

saída:
{
    "status": "SUCCESS",
    "message": "Updated product",
    "data": {
        "id": 115,
        "code": "123412",
        "name": "TESTE DE PRODUTO NOVO",
        "description": "ARROZ COM QUEIJO",
        "stock": 42,
        "price": 21,
        "created_at": "2019-02-27T08:41:23.665Z",
        "updated_at": "2019-02-27T08:59:24.859Z",
        "product_attributes": [
            {
                "id": 106,
                "product_id": 115,
                "description": "SABOR DO PRODUTO!",
                "value": "DELICIOSO",
                "created_at": "2019-02-27T08:41:23.670Z",
                "updated_at": "2019-02-27T08:58:14.204Z"
            },
            {
                "id": 105,
                "product_id": 115,
                "description": "PESO DO PRODUTO",
                "value": "12KG",
                "created_at": "2019-02-27T08:41:23.667Z",
                "updated_at": "2019-02-27T08:41:23.667Z"
            }
        ]
    }
}

Deletar pedido
Cenários de teste:
- Deletar produto que tenha sido feita a venda (não deve ser permitido)

Entrada:
DELETE /api/v1/products/:id

Saída:
{
    "status": "SUCCESS",
    "message": "Deleted product",
    "data": {
        "id": 97,
        "code": "0778406065",
        "name": "Teriyaki Chicken Donburi",
        "description": "Creamy mascarpone cheese and custard layered between espresso and rum soaked house-made ladyfingers, topped with Valrhona cocoa powder.",
        "stock": 14,
        "price": 84.01,
        "created_at": "2019-02-24T10:31:02.692Z",
        "updated_at": "2019-02-24T10:31:02.692Z",
        "product_attributes": []
    }
}
